# Specify service constraints (–constraint)

You can limit the set of nodes where a task can be scheduled by defining constraint expressions. Multiple constraints find nodes that satisfy every expression (AND match). Constraints can match node or Docker Engine labels as follows:


|node attribute|matches|example|
|--------------|-------|-------|
|node.id|Node ID|node.id == 2ivku8v2gvtg4|
|node.hostname|Node hostname|node.hostname != node-2|
|node.role|Node role|node.role == manager|
|node.labels|user defined node labels|node.labels.security == high|
|engine.labels|Docker Engine's labels|engine.labels.operatingsystem == ubuntu 14.04|

engine.labels apply to Docker Engine labels like operating system, drivers, etc. Swarm administrators add node.labels for operational purposes by using the docker node update command.

For example, the following limits tasks for the redis service to nodes where the node type label equals queue:

```
$ docker service create \
  --name redis_2 \
  --constraint 'node.labels.type == queue' \
  redis:3.0.6
```

# Node availability
```
$ docker node update --availability drain node-1
```

Possiblie states:

|State|Meaning|
|-----|-------|
|Active|means that the scheduler can assign tasks to the node.
|Pause|means the scheduler doesn’t assign new tasks to the node, but existing tasks remain running.|
|Drain|means the scheduler doesn’t assign new tasks to the node. The scheduler shuts down any existing tasks and schedules them on an available node.|

# Add label to nodes
```
$ docker node update --label-add foo --label-add bar=baz node-1
```

# Force a rebalance
```
$ docker service update --force <service>
```